# Frankenmap

This repository contains a simple 3d map thingy I made. I used some satellite images from [a website last updated in 2006](https://planetpixelemporium.com/earth8081.html), so I doubt anyone will care.

* The Godot editor mostly doesn't run in the browser ([it kinda does, but it's heavily compromised](https://editor.godotengine.org/releases/latest/)) so you'll need a proper computer to run it.
* This is a Godot project, so you'll need [Godot](godotengine.org) to use it.
* You need Git to download the project to your computer.
* The project needs at least 453 MB of space on-disk for development, and it has similar memory demands at runtime.
* I'm not going to comprehensively document this project since it's only useful for one assignment, so you might need to do some searching if you don't already know Godot.

## How to add your own markers

* Edit [`MainScene`](scenes/MainScene.tscn)
* Select the `position_chooser` node
* Fill the correct index in `Positions` with the latitude, longitude, and altitude of your position, in degrees and Godot units, respectively.
* Fill the correct index in `CameraAngles` with the azimuth, elevation, and range the camera should use in degrees.
* Fill `Titles` and `Blurbs` with human-readable information about your location
* Fill `OrbitSpeed` with orbit speeds in degrees/sec.