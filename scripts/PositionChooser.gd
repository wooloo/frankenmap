extends Control

export var CameraControlPath: NodePath
export var Positions: PoolVector3Array
export var CameraAngles: PoolVector3Array
export var Titles: PoolStringArray
export var Blurbs: PoolStringArray
export var OrbitSpeed: PoolRealArray

onready var cc = get_node(CameraControlPath)

var index = 0

func _ready():
	set_goal()
	pass # Replace with function body.

func set_goal():
	cc.set_goal(Positions[index], CameraAngles[index])
	cc.orbit_speed = OrbitSpeed[index]
	$Panel/PlaceName.text = Titles[index]
	$Panel/Blurb.text = Blurbs[index]

func next():
	index += 1
	index = index % len(Positions)
	set_goal()

func previous():
	index -= 1
	index = index % len(Positions)
	set_goal()
