extends Spatial


# Declare member variables here. Examples:
# var a = 2
# var b = "text"

export var sea_level = 10
export var orbit_speed = 0.0

# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.

var camera_target_lat = 0.0
var camera_target_long = 0.0
var camera_target_alt = 0.0
var camera_azimuth = 0.0
var camera_elevation = -20.0
var camera_range = 1.0
export var movement_factor = 0.05

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	rotation.y = lerp_angle(rotation.y, (camera_target_lat+180)*(PI/180), movement_factor)
	$CameraVertical.rotation.x = lerp_angle($CameraVertical.rotation.x, -camera_target_long*(PI/180), movement_factor)
	$CameraVertical/CameraLookTarget.transform.origin.z = lerp($CameraVertical/CameraLookTarget.transform.origin.z, camera_target_alt + sea_level, movement_factor)
	$CameraVertical/CameraLookTarget.rotation.z = lerp_angle($CameraVertical/CameraLookTarget.rotation.z, camera_azimuth * (PI/180), movement_factor)
	$CameraVertical/CameraLookTarget/CameraLookElevator.rotation.x = lerp_angle($CameraVertical/CameraLookTarget/CameraLookElevator.rotation.x, camera_elevation * (PI/180), movement_factor)
	$CameraVertical/CameraLookTarget/CameraLookElevator/Camera.transform.origin.y = lerp($CameraVertical/CameraLookTarget/CameraLookElevator/Camera.transform.origin.y, -camera_range, movement_factor)
	camera_azimuth += orbit_speed * delta
	pass

func set_goal(tpos, cpos):
	camera_target_lat = tpos.x
	camera_target_long = tpos.y
	camera_target_alt = tpos.z
	camera_azimuth = cpos.x
	camera_elevation = cpos.y
	camera_range = cpos.z
